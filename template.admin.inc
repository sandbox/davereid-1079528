<?php
// $Id: template.admin.inc,v 1.1 2009/03/18 02:00:54 davereid Exp $

/**
 * @file
 * Administrative page callbacks for the template module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function template_settings_form() {

  return system_settings_form($form);
}
